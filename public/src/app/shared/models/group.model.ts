export interface Group {
  _id: string;
  group_name: string;
  group_image;
  workspace_name: string;
  _workspace: string;
  files: string[];
  _members: string[];
  created_date: string;
}
