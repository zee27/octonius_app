export interface User {
  _id: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  workspace_name: string;
  _workspace: string;
  role: string;
  phone_number: string;
  mobile_number: string;
  current_position: string;
  bio: string;
  company_name: string;
  company_join_date: string;
}
